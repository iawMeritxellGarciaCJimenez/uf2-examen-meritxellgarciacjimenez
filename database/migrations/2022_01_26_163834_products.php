<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies',function(Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('category');
            $table->string('descripcion');
            $table->integer('rating');
            $table->integer('stock');            
            $table->double('price',2,2);
            $table->string('image');
            $table->string('created_at')->nullable();
            $table->string('updated_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
