<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User::factory(2)->create();
        User::insert(['id' => '1','name' => 'Pedro','email' => 'pedro@student.com','created_at' => null,'updated_at' => null]);
        User::insert(['id' => '2','name' => 'Sara','email' => 'sara@student.com','created_at' => null,'updated_at' => null]);

    }
}
