<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Product::factory(4)->create();
        Product::insert(['id' => '1','name' => 'El señor de los anillos','category' => '1','descripcion' => 'Algo de un anillo','rating' => '4','stock' => '7','price' => '10.10','img' => 'img01.jpg','created_at' => null,'updated_at' => null]);
        Product::insert(['id' => '2','name' => 'Ironman','category' => '2','descripcion' => 'Un tio con mucha pasta','rating' => '4','stock' => '1','price' => '5.00','img' => 'img02.jpg','created_at' => null,'updated_at' => null]);
        Product::insert(['id' => '3','name' => 'Passengers','category' => '3','descripcion' => 'Una nave espacial','rating' => '2','stock' => '3','price' => '1.00','img' => 'img03.jpg','created_at' => null,'updated_at' => null]);
        Product::insert(['id' => '4','name' => 'V de Vendetta','category' => '2','descripcion' => 'Venganza','rating' => '2','stock' => '3','price' => '1.00','img' => 'img04.jpg','created_at' => null,'updated_at' => null]);
    }
}
